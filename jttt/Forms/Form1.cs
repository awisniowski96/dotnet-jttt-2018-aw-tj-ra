﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;


namespace jttt
{
    public partial class Form1 : Form
    {
        BindingList<Zadanie> lista;

        public Form1()
        {
            InitializeComponent();
            listBox2.Items.Add(DateTime.Now + ": Start aplikacji");
            lista = new BindingList<Zadanie>();

            using (var db = new ZadaniaDbContext())
            {
                foreach (var g in db.Zadanie.Include("warunek").Include("efekt"))

                {
                    
                    lista.Add(g);
                }
            }
            listBox1.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Zadanie zad;
            Effect efekt;
            Condition warunek;

            if (tabControl1.SelectedTab == tabPage1)
            {
                warunek = new ConditionPage(textBox1.Text, textBox2.Text);
                if (tabControl2.SelectedTab == tabPage3)
                    efekt = new EffectEmail(textBox3.Text);
                else
                    efekt = new EffectShow();
            }
            else
            {
                warunek = new ConditionWeather(textBox5.Text, numericUpDown1.Value);
                if (tabControl2.SelectedTab == tabPage3)
                    efekt = new EffectEmail(textBox3.Text);
                else
                    efekt = new EffectShow();
            }

            zad = new Zadanie(warunek, efekt, textBox4.Text);

            using (var db = new ZadaniaDbContext())
            {
                db.Zadanie.Add(zad);
                db.SaveChanges();

                try
                {
                    lista.Add(zad);
                    listBox2.Items.Add(DateTime.Now + ": Dodano zadanie:" + zad.ToString());
                }
                catch (Exception)
                {
                    MessageBox.Show("Cos poszlo nie tak");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (var db = new ZadaniaDbContext())
            {
                db.Database.ExecuteSqlCommand("DELETE FROM Zadanies WHERE 1=1");
                db.Database.ExecuteSqlCommand("DELETE FROM Conditions WHERE 1=1");
                db.Database.ExecuteSqlCommand("DELETE FROM Effects WHERE 1=1");
                db.SaveChanges();
            }
            lista.Clear();
            listBox2.Items.Add(DateTime.Now + ": Wyczyszczono listę zadań");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // try
            //{
            if (lista.Count == 0)
            {
                MessageBox.Show("Brak zadan do wykonania");
                return;
            }
            foreach (Zadanie item in lista)
            {
                item.Wykonaj();
                listBox2.Items.Add(DateTime.Now + ": Wykonano zadanie:" + item.ToString());
            }
            MessageBox.Show("Zrobione!");
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("Cos poszlo nie tak");
            //}
        }

    }
}
