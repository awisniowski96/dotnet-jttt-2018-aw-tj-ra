﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jttt
{
    public partial class Form2 : Form
    {
        public string Obrazek { get; set; }

        public Form2(string data, string obraz)
        {
            InitializeComponent();
            Obrazek = obraz;
            Image tmp = Image.FromFile(Obrazek);
            Image obrazek = new Bitmap(tmp);
            tmp.Dispose();
            pictureBox1.Image = obrazek;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

            richTextBox1.Text = data;
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            File.Delete(Obrazek);
        }
    }
}
