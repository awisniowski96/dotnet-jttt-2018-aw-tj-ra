﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    public abstract class Condition
    {
        public Condition()
        {

        }
        public int ConditionId { get; set; }
        public abstract bool CheckCondition();
        public abstract string GetData();
        public abstract string GetPicture();
    }
}
