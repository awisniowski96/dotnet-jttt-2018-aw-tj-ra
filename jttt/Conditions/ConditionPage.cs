﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    public class ConditionPage: Condition
    {
        public string Url { get; set; }
        public string Tag { get; set; }

        public ConditionPage(string url, string tag)
        {
            Url = url;
            Tag = tag;
        }


        public ConditionPage()
        {

        }
        public override bool CheckCondition()
        {
            var doc = new HtmlDoc(Url);
            var nodes = doc.DocumentNode.Descendants("img");
            if (doc.CheckNodes(nodes, Tag))
            {
                return true;
            }
            else
                return false;
        }

        public override string GetData()
        {
            return Tag;
        }

        public override string GetPicture()
        {
            return "obrazek.jpg";
        }
        
        public override string ToString()
        {
            return "URL: " + Url + "TAG: " + Tag;
        }
    }
}
