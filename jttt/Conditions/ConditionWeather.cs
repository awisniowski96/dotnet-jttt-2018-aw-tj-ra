﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    public class ConditionWeather : Condition
    {
        public string Miasto { get; set; }
        public decimal Temp { get; set; }
        public string APIKey = "0219a0183d07b81639ee544fc7f85589";
        public string Pogoda { get; set; }
        WeatherInfo.RootObject data;

        public ConditionWeather(string miasto, decimal temp)
        {
            Miasto = miasto;
            Temp = temp;
        }

        public ConditionWeather()
        {

        }

        public override string GetData()
        {
            return Pogoda;
        }

        public override string GetPicture()
        {
            return "pogoda.jpg";
        }

        public override bool CheckCondition()
        {
            using (var webClient = new WebClient())
            {
                string url = string.Format("http://api.openweathermap.org/data/2.5/weather?q={0},pl&APPID={1}", Miasto, APIKey);
                var json = webClient.DownloadString(url);

                data = JsonConvert.DeserializeObject<WeatherInfo.RootObject>(json);
                WeatherInfo.RootObject info = data;

                Pogoda = string.Format("W miescie {0}, temperatura wynosi: {1} stopni Celcjusza, cisnienie wynosi: {2} hPa, zachmurzenie w %: {3}",
                       data.name, data.main.temp - 273, data.main.pressure, data.clouds.all);

                if ((data.main.temp - 273) >= (double)Temp)
                {
                    webClient.DownloadFile(string.Format("http://openweathermap.org/img/w/{0}.png", data.weather[0].icon), "pogoda.jpg");
                    return true;
                }
                else
                    return false;
            }
        }
        /*
        public override string ToString()
        {
            return "MIASTO: " + Miasto + " TEMP: " + Temp;
        }*/

    }
}
