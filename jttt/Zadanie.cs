﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    [Serializable]
    public class Zadanie
    {
        public int Id { get; set; }
        public int? ConditionId { get; set; }
        public int? EffectId { get; set; }

        [ForeignKey("ConditionId")]
        public virtual Condition warunek { get; set; }
        [ForeignKey("EffectId")]
        public virtual Effect efekt { get; set; }
        public string Nazwa { get; set; }

        public Zadanie(Condition war, Effect ef, string nazw)
        {
            warunek = war;
            efekt = ef;
            Nazwa = nazw;
        }
        public Zadanie() { }

        public void Wykonaj()
        {
            if (warunek.CheckCondition())
                efekt.Do(warunek.GetData(),warunek.GetPicture());
        }
        
        public override string ToString()
        {
            return "TASK: " + Nazwa + " " + warunek.ToString() + " " + efekt.ToString();  
        }
    }
}
