﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    public abstract class Effect
    {
        public Effect()
        {

        }
        public int EffectId { get; set; }
        public abstract void Do(string data, string obrazek);
    }
}
