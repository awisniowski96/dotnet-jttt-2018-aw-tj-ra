﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    public class EffectEmail : Effect
    {
        public string Email { get; set; }

        public EffectEmail()
        {

        }
        public EffectEmail(string email)
        {
            Email = email;
        }

        public override void Do(string data, string obrazek)
        {
            Mail.SendEmail(Email, data, obrazek);
        }
        /*
        public override string ToString()
        {
            return "EMAIL: " + Email;
        }*/
    }
}
