﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    class HtmlDoc : HtmlDocument
    {

        public HtmlDoc(string url)
        {
            this.LoadHtml(GetHtml(url));
        }

        public string GetHtml(string url)
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(url));

                return html;
            }
        }

        public bool CheckNodes(IEnumerable<HtmlNode> nodes, string tag)
        {
            
            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").ToLower().Contains(tag.ToLower()))
                {
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(node.GetAttributeValue("src", ""), "obrazek.jpg");
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
